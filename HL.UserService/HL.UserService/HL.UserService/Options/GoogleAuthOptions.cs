﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HL.UserService.Options
{
    public class GoogleAuthOptions
    {
        public string ClientId { get; set; }

        public string ClientSecret { get; set; }
    }
}
