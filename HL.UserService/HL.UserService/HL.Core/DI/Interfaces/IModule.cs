﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace HL.Core.DI.Interfaces
{
    public interface IModule
    {
        void AddServices(IServiceCollection services, IConfiguration configuration);
    }
}
